# set_reolink_time

This is a small script to set the time of a Reolink IP camera via its API.
It has been designed for my personal use and might not do what you want.
Check out [reolinkapipy](https://github.com/ReolinkCameraAPI/reolinkapipy) for a more complete implementation.

I will probably not accept contributions for this script, but feel free to try.

## Requirements

`requests` needs to be installed.

## Usage

See `set_reolink_time.py --help`.
