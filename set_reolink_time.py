#!/usr/bin/env python3

from datetime import datetime
from json import JSONDecodeError
from os import environ
import time

import requests


class APIError(Exception):
    pass


class Camera:
    def __init__(self, addr: str):
        self.url = f"http://{addr}/cgi-bin/api.cgi"
        self.session = requests.Session()

    def post(self, *args, **kwargs):
        res = self.session.post(*args, **kwargs)
        if res is None:
            raise APIError("received no response")
        res.raise_for_status()
        try:
            data = res.json()
        except JSONDecodeError as e:
            raise APIError(f"invalid JSON, response was: {res.text}")
        if not isinstance(data, list):
            raise APIError(f"response is not a list, but {data}")
        if len(data) < 1:
            raise APIError("response is an empty list")
        if "code" not in data[0]:
            raise APIError("response doesn't have `code`")
        if int(data[0]["code"]) != 0:
            raise APIError(f"got code {data[0]['code']}")
        return data[0]

    def login(self, user: str, passwd: str):
        params = { "cmd": "Login", "token": "null" }
        body = [{
            "cmd": "Login", "action": 0,
            "param": {
                "User": {
                    "userName": user,
                    "password": passwd,
                },
            },
        }]
        data = self.post(self.url, params=params, json=body)
        if "value" not in data:
            raise APIError("response has no value")
        if "Token" not in data["value"]:
            raise APIError("response value has no `Token`")
        if "name" not in data["value"]["Token"]:
            raise APIError("token has no `name`")
        self.session.params["token"] = data["value"]["Token"]["name"]

    def set_time(self, dt: datetime = None):
        if dt is None:
            dt = datetime.now()
        body = [{
            "cmd": "SetTime", "action": 0,
            "param": {
                "Time": {
                    "year": dt.year,
                    "mon": dt.month,
                    "day": dt.day,
                    "hour": dt.hour,
                    "min": dt.minute,
                    "sec": dt.second,
                    "timeZone": time.timezone,
                    "timeFmt": "YYYY/MM/DD",
                    "hourFmt": 0,
                },
            },
        }]
        self.post(self.url, json=body)

    def set_dst(self):
        body = [{
            "cmd": "SetTime", "action": 0,
            "param": {
                "Dst": {
                    # This is exactly what my browser's Network tab captured.
                    # It basically describes the rule as it is in Germany (and
                    # most other European countries): DST starts on the last
                    # Sunday in March at 02:00 and ends on the last Sunday in
                    # October at 02:00.
                    # This information can possibly be retrieved using pytz:
                    # <https://stackoverflow.com/a/7594755/417040>
                    # I am not sure whether "enable" means "it's DST now" or
                    # "this country is generally using DST". I'll assume the
                    # latter and leave this at 1. Let me know if you live
                    # somewhere else and you've captured a request with a
                    # different value.
                    "enable": 1,
                    "endHour": 2,
                    "endMin": 59,
                    "endMon": 10,
                    "endSec": 0,
                    "endWeek": 5,
                    "endWeekday": 0,
                    # I'm assuming "offset" defines the number of hours between
                    # DST and normal time. Again, let me know if you've
                    # observed something different.
                    "offset": 1,
                    "startHour": 1,
                    "startMin": 59,
                    "startMon": 3,
                    "startSec": 0,
                    "startWeek": 4,
                    "startWeekday": 0,
                },
            },
        }]
        self.post(self.url, json=body)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Set the time of a Reolink IP camera.",
        epilog="Note that you have to provide user name and password via "
            "environment variables: REOLINK_USER and REOLINK_PASS, "
            "respectively. REOLINK_USER defaults to `admin` though.",
    )
    parser.add_argument("address",
        type=str,
        help="host name or IP address of the camera",
    )

    args = parser.parse_args()
    c = Camera(args.address)
    c.login(environ.get("REOLINK_USER", "admin"), environ["REOLINK_PASSWORD"])
    c.set_dst()
    c.set_time()
    print(f'Time for camera "{args.address}" updated successfully.')
